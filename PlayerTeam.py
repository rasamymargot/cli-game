
from _typeshed import Self
import sys
import argparse
from typing import Iterable, Protocol
from dataclasses import dataclass

from person import Person

@dataclass
class PlayerTeam :

    _members : Iterable[Person]
    __nb_warriors : int
    __nb_hunters : int
    __nb_wizatds : int
    __damage : int
    __loot : int
    __flee : int

    def get_degat(self) -> int :
        return self.__damage
    
    def get_fuite(self) -> int:
        return self.__flee
    
    def get_nomber_warrior(self) -> int:
        return self.__nb_warriors
    
    def get_nomber_hunter(self) -> int:
        return self.__nb_hunters

    def get_nomber_wizatds(self) -> int:
        return self.__nb_wizatds

