from typing import Protocol

class Person(Protocol):
    # Degat
    def damage(self)-> int:
        pass

    # Chance
    def luck(self)-> int:
        pass

    # Fuite
    def flee(self)-> int:
        pass

    # Prix
    def price(self)-> int:
        pass

    # Type d'unite
    def unit(self)-> str:
        pass
        